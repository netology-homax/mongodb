let MongoClient = require('mongodb').MongoClient;
const config = require('./config');
const data = require('./data');

// Use connect method to connect to the Server
MongoClient.connect(config.mongodb.connect, function(err, db) {
  if (err) {
    throw err;
  }
  console.log("Connected correctly to server");

  Promise.resolve()
    .then(() => insertUsers(db, data))
    .then(result => {
      console.log(`Inserted ${result.result.n} documents into the ${config.mongodb.collection} collection`);
    })
    .then(() => findUsers(db))
    .then(users => {
      console.log('Found following users:');
      users.forEach(user => console.log(user));
    })
    .then(() => deleteUsers(db))
    .then(() => {
      console.log('All users deleted');
    })
    .catch(console.log.bind(console))
    .then(() => {
      console.log('Close db');
      db.close();
    });
});

function insertUsers(db, data) {
  return new Promise((resolve, reject) => {
      let collection = db.collection(config.mongodb.collection);
      collection.insertMany(data, (err, result) => {
          if (err) {
            reject(err);
            return;
          }
          if (result.result.n !== data.length) {
            reject('Not all records inserted');
            return;
          }
          if (result.ops.length !== data.length) {
            reject('Wrong operation count');
            return;
          }
          resolve(result);
      });
  });
}

function findUsers(db) {
  return new Promise((resolve, reject) => {
    let collection = db.collection(config.mongodb.collection);
    collection.find({}).toArray((err, users) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(users);
    });
  });
}

function deleteUsers(db) {
  return new Promise((resolve, reject) => {
    let collection = db.collection(config.mongodb.collection);
    collection.remove();
    resolve();
  });
}
